document.getElementById("customRange1").onchange = getNumber;


function getNumber(e) {
    const count = e.target.value;
    console.log(e.target.getAttribute('max'))
    console.log(count);
}

function generateTableAsLoop() {
    const count = document.getElementById("customRange1").value
    for (let i=0; i<count; i++) {
        const xhr = new XMLHttpRequest();
        // https://randomuser.me/api/?results=5
        xhr.open('GET', 'https://randomuser.me/api/');
        xhr.responseType = "json";
        xhr.send();
        console.log('Sent');
        xhr.onload = function() {
            let resp = xhr.response;
            console.log(resp)
        }
    }
}

function generateTable() {
    const count = document.getElementById("customRange1").value
    const xhr = new XMLHttpRequest();
    const url = `https://randomuser.me/api/?results=${count}`
    xhr.open('GET', url);
    xhr.responseType = "json";
    xhr.send();
    console.log('Sent');
    xhr.onload = function() {
        let resp = xhr.response;
        const bodyEl = document.getElementById("content");
        bodyEl.innerHTML = '';
        for (let user of resp.results) {
            const f_name = user.name.first;
            const l_name = user.name.first;
            const email = user.email;
            const nameEl = document.createElement('td')
            nameEl.innerText = f_name
            const lnameEl = document.createElement('td')
            lnameEl.innerText = l_name
            const emailEl = document.createElement('td')
            emailEl.innerText = email
            const rowEl = document.createElement('tr')
            rowEl.appendChild(nameEl);
            rowEl.appendChild(lnameEl);
            rowEl.appendChild(emailEl);
            bodyEl.appendChild(rowEl);
        }
    }
}

let name_undf;
let name_null = null;
parseInt("a");
let name = 'String';
let format_name = 'String1'+ 'string 2';
let inline_format_name = `Dynamic: ${name}`
let intN = 1;
let floatN = 2.0;
let array = ['1', 3, null];
let array2 = new Array(3);
array2.push(1);
array2.push(5)
array2[1] = 10;

array2[array2.length-1];
let obj = {a: intN, b:floatN, c:array}
obj.d = format_name;
obj['g'] = inline_format_name


function f(a,b,c,...data) {
    let letValue = 40;
    {
        let letValue = 10;
        var varValue = 20;
        epicValue = 30;
        console.log('letValue f in' + letValue);
    }
    console.log('letValue f out' + letValue);
    console.log('varValue' + varValue);
    console.log('epicValue' + epicValue);

    console.log(a);
    console.log(b);
    console.log(c);
    console.log(data);
    console.log(arguments);
}

const rowEl = document.createElement('h1')
rowEl.innerText = 'asdasdasda';
rowEl.onclick = (e) => {console.log(e.target)}; //arrow function
document.body.appendChild(rowEl);


let items = ['A', "B", "C"];
items.forEach(item => {alert(item)})

var alertItem = function(item){
    alert(item);
}
items.forEach(alertItem)

for(let index = 0; index<10;){
    console.log(index);
    index++;
}